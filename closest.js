(function ($, MAX, MIN, SQT){
    "use strict";

    /*
     * just a quick dynamic style to be added
     *
     * returns nothing
     */
    var addStyle = function() {
        var $style = $('<style>.highlight_nearest{border: 1px solid #FF0000 !important;}</style>');
        $('body').append($style);
    }

    /*
     * param selector   - (mixed) selector to be searched for (usually 'a')
     * param x          - (int) the x coordinate given
     * param y          - (int) the y coordinate given
     * param visibility - (bool) whether the element should be visibile or not
     * param pixels     - (int) pixels away from
     *
     * should return an array of the closest potential selectors to highlight
     */
    var closestSelectors = function (selector, x, y, visibility, pixels) {
        var $selector   = $(selector),
            list        = [],
            found       = [],
            compare     = Infinity;

        x           = parseInt(x) || 0;
        y           = parseInt(y) || 0;
        visibility  = (visibility === true);
        pixels      = parseInt(pixels) || 1;

        $selector.each(function (index, item) {
            var _self       = item,
                $self       = $(item),
                offset      = $self.offset(),
                top         = offset.top,
                left        = offset.left,
                height      = $self.height(),
                width       = $self.width(),
                bottom      = top + height,
                right       = left + width,
                max_top     = MAX(top, y),
                max_left    = MAX(left, x),
                max_bottom  = MIN(bottom, y),
                max_right   = MIN(right, x),

                dy = (max_bottom >= max_top)
                    ? 0
                    : max_top - max_bottom,
                dx = (max_right >= max_left)
                    ? 0
                    : max_left - max_right,
                d = (max_bottom >= max_top || max_right >= max_left)
                    ? MAX(dx, dy)
                    : SQT(dx*dx + dy*dy),
                push;

            // if good add to the list
            if(d <= compare + pixels) {
                if(visibility && $self.css('visibility') == 'hidden'){
                    return;
                }
                compare = MIN(compare, d);
                push = {
                    selector: _self,
                    distance: d
                }
                list.push(push)
            }
        })

        // if there's anything in the list, iterate and calculate, push if valid
        $.each(list, function(index,item) {
            if(item.distance >= compare && item.distance <= (compare + pixels)) {
                found.push(item.selector);
            }
        })

        return found;
    }

    /*
     * actions to take place on domready init
     *
     * removes any click actions (prevents anchor click throughs)
     * removes any highlighted links
     * find closest links
     * hightlight closest links
     */
    $(function() {
        addStyle();

        $('body').on('click', function(e){
            e.preventDefault();
            $('.highlight_nearest').removeClass('highlight_nearest')
            var closest_found = closestSelectors('a', e.pageX, e.pageY, false, 1);
            $.each(closest_found, function(index,item){
                $(item).addClass('highlight_nearest')
            })
        });
    })

})(jQuery, Math.max, Math.min, Math.sqrt)
